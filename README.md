# Flux Base Repo

This repo includes the Flux source manifests, plus bootstraps for Flux GitRepositories, Kustomizations and HelmReleases.

Deployment notifications are send to Slack via webhook.

## SOPS

Secrets can be SOPS encrypted as follows:

    sops --encrypt --in-place clientauth-ca-Secret.yaml